#include <stdio.h>
#include <pthread.h>
//Thomas Aranguiz - Josefa Pinto

//Esta funcion se encargar de imprimir 5 enteros consecutivos para cada hebra.
//El numero de inicio y fin se calcula en funcion del identificador de la hebra dentro del ciclo for.
void* imprimir_enteros(void* arg) {
    int hilo_id = *(int*) arg;
    int i, inicio, fin;

    inicio = hilo_id * 5 + 1;
    fin = inicio + 4;

    for (i = inicio; i <= fin; i++) {
        printf("Hilo %d impresiones %d\n", hilo_id, i);
    }

    pthread_exit(NULL);
}


//Dentro del main se crean las 5 hebras utilizando un ciclo for y se crea un arreglo donde se guarda los id de cada hebras.
//despues de crear las hebras, el programa espera a que cada hebra termine su ejecucion usando el "join". y termina la simulacion.
int main() {
    int i;
    pthread_t hebras[5];
    int id_hebras[5];

    for (i = 0; i < 5; i++) {
        id_hebras[i] = i;
        pthread_create(&hebras[i], NULL, imprimir_enteros, &id_hebras[i]);
    }

    for (i = 0; i < 5; i++) {
        pthread_join(hebras[i], NULL);
    }

    return 0;
}
