#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
//Thomas Aranguiz - Josefa Pinto

//variables globales.
int impresiones = 0;
sem_t semaforo;

//Esta funcion se encarga de imprimir el valor actual de impresiones y luego incrementarlo.
//Antes de acceder al valor se utiliza un semaforo, para obtener el valor actual de "impresiones", luego se libera la hebra.
void* imprimir_enteros(void* arg) {
    int i;
    sem_wait(&semaforo);
    for (i = 0; i < 5; i++) {
        printf("hilo %d - impresion %d\n", i, impresiones+1);
        impresiones++;
    }
    sem_post(&semaforo);
    pthread_exit(NULL);
}

//Se crea un semaforo, cinco hebras en un bucle, todas las hebras pasan por el void y se espera su ejecucion con el join.
//Al final se destruye el semaforo y el programa termina.
int main() {
    int i;
    pthread_t hebras[5];

    sem_init(&semaforo, 0, 1);

    for (i = 0; i < 5; i++) {
        pthread_create(&hebras[i], NULL, imprimir_enteros, NULL);
    }

    for (i = 0; i < 5; i++) {
        pthread_join(hebras[i], NULL);
    }

    sem_destroy(&semaforo);

    return 0;
}
