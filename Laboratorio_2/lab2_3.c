#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <pthread.h>


void *Funson3(void *name){

    char *nombre = (char*)name;

    printf(" Se creo %s\n",nombre);
    printf(" Se acabo el proceso de %s\n",nombre);

    return NULL;
}


void *Funson2(void *name){

    char *nombre = (char*)name;
    printf(" Se creo %s\n",nombre);

    pthread_t son2;

    pthread_create(&son2, NULL, Funson3, "Nieto");
    pthread_join(son2, NULL);

    printf(" Se acabo el proceso de %s\n",nombre);
    return NULL;
}


void *Funson(void *name){

    char *nombre = (char*)name;
    printf(" Se creo %s\n",nombre);

    pthread_t son1;

    pthread_create(&son1, NULL, Funson2, "Hijo");
    pthread_join(son1, NULL);

    printf(" Se acabo el proceso de %s\n",nombre);

    return NULL;
}


int main(void){

    pthread_t   hebra1;

    pthread_create(&hebra1, NULL, Funson, "Padre");
    pthread_join(hebra1, NULL);

    return 0;
}
