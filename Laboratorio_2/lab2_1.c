#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <pthread.h>


void *FuncionThead(void *name){
    char *nombre = (char*)name;
    printf("se creo %s\n",nombre);
    return NULL;
}


int main(void){

    pthread_t   hebra1,
                hebra2,
                hebra3,
                hebra4,
                hebra5,
                hebra6,
                hebra7,
                hebra8;

    pthread_create(&hebra1, NULL, FuncionThead, "hebra_1");
    pthread_create(&hebra2, NULL, FuncionThead, "hebra_2");
    pthread_create(&hebra3, NULL, FuncionThead, "hebra_3");
    pthread_create(&hebra4, NULL, FuncionThead, "hebra_4");
    pthread_create(&hebra5, NULL, FuncionThead, "hebra_5");
    pthread_create(&hebra6, NULL, FuncionThead, "hebra_6");
    pthread_create(&hebra7, NULL, FuncionThead, "hebra_7");
    pthread_create(&hebra8, NULL, FuncionThead, "hebra_8");

    sleep(10)
    return 0;
}
