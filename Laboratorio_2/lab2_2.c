#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <pthread.h>


void *FuncionThead(void *name){
    char *nombre = (char*)name;
    printf("%s Creado!\n",nombre);
    return NULL;
}


int main(void){

    pthread_t   hebra1,
                hebra2,
                hebra3;

    pthread_create(&hebra1, NULL, FuncionThead, "hebra_1");
    pthread_create(&hebra2, NULL, FuncionThead, "hebra_2");
    pthread_create(&hebra3, NULL, FuncionThead, "hebra_3");

    sleep(10);
    return 0;
}
